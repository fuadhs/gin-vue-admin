package api

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/common/response"
{{ if .NeedModel }}	"github.com/flipped-aurora/gin-vue-admin/server/plugin/{{ .Snake}}/model" {{ end }}
	"github.com/flipped-aurora/gin-vue-admin/server/plugin/{{ .Snake}}/service"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type {{ .PlugName}}Api struct{}

// @Tags {{ .PlugName}}
// @Summary Please manually fill in the interface function
// @Produce  application/json
// @Success 200 {string} string "{"success":true,"data":{},"msg":"Sent successfully"}"
// @Router /{{ .RouterGroup}}/routerName [post]
func (p *{{ .PlugName}}Api) ApiName(c *gin.Context) {
    {{ if .HasRequest}}
        var plug model.Request
        _ = c.ShouldBindJSON(&plug)
    {{ end }}
        if {{ if .HasResponse }} res, {{ end }} err:= service.ServiceGroupApp.PlugService({{ if .HasRequest }}plug{{ end -}}); err != nil {
		global.GVA_LOG.Error("failed!", zap.Error(err))
		response.FailWithMessage("failed", c)
	} else {
	{{if .HasResponse }}
	    response.OkWithDetailed(res,"success",c)
	{{else}}
	    response.OkWithData("success", c)
	{{ end -}}

	}
}
