## server project structure

```shell
├── api
│   └── v1
├── config
├── core
├── docs
├── global
├── initialize
│   └── internal
├── middleware
├── model
│   ├── request
│   └── response
├── packfile
├── resource
│   ├── excel
│   ├── page
│   └── template
├── router
├── service
├── source
└── utils
    ├── timer
    └── upload
```

| folder       | 说明                    | desc                        |
| ------------ | ----------------------- | --------------------------- |
| `api`        | api层                   | api层 |
| `--v1`       | v1版本接口              | v1版本接口                  |
| `config`     | 配置包                  | config.yaml对应的配置structure |
| `core`       | 核心文件                | 核心组件(zap, viper, server)的初始化 |
| `docs`       | swaggerDocument目录         | swaggerDocument目录 |
| `global`     | 全局对象                | 全局对象 |
| `initialize` | 初始化 | router,redis,gorm,validator, timer的初始化 |
| `--internal` | 初始化内部函数 | gorm 的 longger 自定义,在此folder的函数只能由 `initialize` 层进行调用 |
| `middleware` | 中间件层 | 用于存放 `gin` 中间件代码 |
| `model`      | 模型层                  | 模型对应数据表              |
| `--request`  | 入参structure              | 接收前端发送到后端的数据。  |
| `--response` | 出参structure              | Return给前端的数据structure      |
| `packfile`   | 静态文件打包            | 静态文件打包 |
| `resource`   | 静态资源folder          | 负责存放静态文件                |
| `--excel` | excel导入导出默认path | excel导入导出默认path |
| `--page` | Form Builder | Form Builder 打包后的dist |
| `--template` | 模板 | 模板folder,存放的YesCode Generator的模板 |
| `router`     | 路由层                  | 路由层 |
| `service`    | service层               | 存放业务逻辑问题 |
| `source` | source层 | 存放初始化数据的函数 |
| `utils`      | 工具包                  | 工具函数封装            |
| `--timer` | timer | 定时器接口封装 |
| `--upload`      | oss                  | oss接口封装        |

