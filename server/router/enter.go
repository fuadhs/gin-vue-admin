package router

import (
	"github.com/flipped-aurora/gin-vue-admin/server/router/MasterBidang"
	"github.com/flipped-aurora/gin-vue-admin/server/router/example"
	"github.com/flipped-aurora/gin-vue-admin/server/router/system"
)

type RouterGroup struct {
	System       system.RouterGroup
	Example      example.RouterGroup
	MasterBidang MasterBidang.RouterGroup
}

var RouterGroupApp = new(RouterGroup)
