package system

import (
	v1 "github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/gin-gonic/gin"
)

type AutoCodeHistoryRouter struct{}

func (s *AutoCodeRouter) InitAutoCodeHistoryRouter(Router *gin.RouterGroup) {
	autoCodeHistoryRouter := Router.Group("autoCode")
	autoCodeHistoryApi := v1.ApiGroupApp.SystemApiGroup.AutoCodeHistoryApi
	{
		autoCodeHistoryRouter.POST("getMeta", autoCodeHistoryApi.First)         // According to idGet meta information
		autoCodeHistoryRouter.POST("rollback", autoCodeHistoryApi.RollBack)     // rollback
		autoCodeHistoryRouter.POST("delSysHistory", autoCodeHistoryApi.Delete)  // delete rollback record
		autoCodeHistoryRouter.POST("getSysHistory", autoCodeHistoryApi.GetList) // Get rollbackRecord pagination
	}
}
