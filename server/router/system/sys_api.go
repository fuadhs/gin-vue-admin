package system

import (
	v1 "github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type ApiRouter struct{}

func (s *ApiRouter) InitApiRouter(Router *gin.RouterGroup) {
	apiRouter := Router.Group("api").Use(middleware.OperationRecord())
	apiRouterWithoutRecord := Router.Group("api")
	apiRouterApi := v1.ApiGroupApp.SystemApiGroup.SystemApiApi
	{
		apiRouter.POST("createApi", apiRouterApi.CreateApi)               // Create APIs
		apiRouter.POST("deleteApi", apiRouterApi.DeleteApi)               // Delete APIs
		apiRouter.POST("getApiById", apiRouterApi.GetApiById)             // Get a single Api message
		apiRouter.POST("updateApi", apiRouterApi.UpdateApi)               // update api
		apiRouter.DELETE("deleteApisByIds", apiRouterApi.DeleteApisByIds) // delete selected api
	}
	{
		apiRouterWithoutRecord.POST("getAllApis", apiRouterApi.GetAllApis) // get all APIs
		apiRouterWithoutRecord.POST("getApiList", apiRouterApi.GetApiList) // Get API list
	}
}
