package system

import (
	v1 "github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/gin-gonic/gin"
)

type AutoCodeRouter struct{}

func (s *AutoCodeRouter) InitAutoCodeRouter(Router *gin.RouterGroup) {
	autoCodeRouter := Router.Group("autoCode")
	autoCodeApi := v1.ApiGroupApp.SystemApiGroup.AutoCodeApi
	{
		autoCodeRouter.GET("getDB", autoCodeApi.GetDB)                  // get database
		autoCodeRouter.GET("getTables", autoCodeApi.GetTables)          // Get the table corresponding to the database
		autoCodeRouter.GET("getColumn", autoCodeApi.GetColumn)          // Get all field information of the specified table
		autoCodeRouter.POST("preview", autoCodeApi.PreviewTemp)         // Get automatic create code preview
		autoCodeRouter.POST("createTemp", autoCodeApi.CreateTemp)       // createAutomation code
		autoCodeRouter.POST("createPackage", autoCodeApi.CreatePackage) // create package
		autoCodeRouter.POST("getPackage", autoCodeApi.GetPackage)       // get package
		autoCodeRouter.POST("delPackage", autoCodeApi.DelPackage)       // delete package
		autoCodeRouter.POST("createPlug", autoCodeApi.AutoPlug)         // Auto Plugin Package Template
		autoCodeRouter.POST("installPlugin", autoCodeApi.InstallPlugin) // automatic install plugin
	}
}
