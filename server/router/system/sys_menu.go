package system

import (
	v1 "github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type MenuRouter struct{}

func (s *MenuRouter) InitMenuRouter(Router *gin.RouterGroup) (R gin.IRoutes) {
	menuRouter := Router.Group("menu").Use(middleware.OperationRecord())
	menuRouterWithoutRecord := Router.Group("menu")
	authorityMenuApi := v1.ApiGroupApp.SystemApiGroup.AuthorityMenuApi
	{
		menuRouter.POST("addBaseMenu", authorityMenuApi.AddBaseMenu)           // new menu
		menuRouter.POST("addMenuAuthority", authorityMenuApi.AddMenuAuthority) //	Add menu and role association
		menuRouter.POST("deleteBaseMenu", authorityMenuApi.DeleteBaseMenu)     // delete menu
		menuRouter.POST("updateBaseMenu", authorityMenuApi.UpdateBaseMenu)     // update menu
	}
	{
		menuRouterWithoutRecord.POST("getMenu", authorityMenuApi.GetMenu)                   // get menu tree
		menuRouterWithoutRecord.POST("getMenuList", authorityMenuApi.GetMenuList)           // Paging to get the basic menu list
		menuRouterWithoutRecord.POST("getBaseMenuTree", authorityMenuApi.GetBaseMenuTree)   // Get user dynamic routing
		menuRouterWithoutRecord.POST("getMenuAuthority", authorityMenuApi.GetMenuAuthority) // Get the specified role menu
		menuRouterWithoutRecord.POST("getBaseMenuById", authorityMenuApi.GetBaseMenuById)   // get menu by id
	}
	return menuRouter
}
