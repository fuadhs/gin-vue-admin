package system

import (
	v1 "github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type UserRouter struct{}

func (s *UserRouter) InitUserRouter(Router *gin.RouterGroup) {
	userRouter := Router.Group("user").Use(middleware.OperationRecord())
	userRouterWithoutRecord := Router.Group("user")
	baseApi := v1.ApiGroupApp.SystemApiGroup.BaseApi
	{
		userRouter.POST("admin_register", baseApi.Register)               // Administrator registered account
		userRouter.POST("changePassword", baseApi.ChangePassword)         // user change password
		userRouter.POST("setUserAuthority", baseApi.SetUserAuthority)     // Set user permissions
		userRouter.DELETE("deleteUser", baseApi.DeleteUser)               // delete users
		userRouter.PUT("setUserInfo", baseApi.SetUserInfo)                // Set user information
		userRouter.PUT("setSelfInfo", baseApi.SetSelfInfo)                // set self information
		userRouter.POST("setUserAuthorities", baseApi.SetUserAuthorities) // Set user permissions group
		userRouter.POST("resetPassword", baseApi.ResetPassword)           // reset user permissions group
	}
	{
		userRouterWithoutRecord.POST("getUserList", baseApi.GetUserList) // Paging to get user list
		userRouterWithoutRecord.GET("getUserInfo", baseApi.GetUserInfo)  // get self information
	}
}
