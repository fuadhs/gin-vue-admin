package example

import (
	v1 "github.com/flipped-aurora/gin-vue-admin/server/api/v1"
	"github.com/flipped-aurora/gin-vue-admin/server/middleware"
	"github.com/gin-gonic/gin"
)

type CustomerRouter struct{}

func (e *CustomerRouter) InitCustomerRouter(Router *gin.RouterGroup) {
	customerRouter := Router.Group("customer").Use(middleware.OperationRecord())
	customerRouterWithoutRecord := Router.Group("customer")
	exaCustomerApi := v1.ApiGroupApp.ExampleApiGroup.CustomerApi
	{
		customerRouter.POST("customer", exaCustomerApi.CreateExaCustomer)   // create customer
		customerRouter.PUT("customer", exaCustomerApi.UpdateExaCustomer)    // update customer
		customerRouter.DELETE("customer", exaCustomerApi.DeleteExaCustomer) // delete customer
	}
	{
		customerRouterWithoutRecord.GET("customer", exaCustomerApi.GetExaCustomer)         // Get single customer information
		customerRouterWithoutRecord.GET("customerList", exaCustomerApi.GetExaCustomerList) // Get a list of customers
	}
}
