package utils

import (
	"archive/zip"
	"io"
	"os"
	"strings"
)

//@author: [piexlmax](https://github.com/piexlmax)
//@function: ZipFiles
//@description: Compressed file
//@param: filename string, files []string, oldForm, newForm string
//@return: error

func ZipFiles(filename string, files []string, oldForm, newForm string) error {
	newZipFile, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer func() {
		_ = newZipFile.Close()
	}()

	zipWriter := zip.NewWriter(newZipFile)
	defer func() {
		_ = zipWriter.Close()
	}()

	// add files to zip
	for _, file := range files {

		err = func(file string) error {
			zipFile, err := os.Open(file)
			if err != nil {
				return err
			}
			defer zipFile.Close()
			// Get the basic information of the file
			info, err := zipFile.Stat()
			if err != nil {
				return err
			}

			header, err := zip.FileInfoHeader(info)
			if err != nil {
				return err
			}

			// Use the above FileInforHeader() to replace the path where the file is saved with what we want, as follows
			header.Name = strings.Replace(file, oldForm, newForm, -1)

			// optimized compression
			// For more reference see http://golang.org/pkg/archive/zip/#pkg-constants
			header.Method = zip.Deflate

			writer, err := zipWriter.CreateHeader(header)
			if err != nil {
				return err
			}
			if _, err = io.Copy(writer, zipFile); err != nil {
				return err
			}
			return nil
		}(file)
		if err != nil {
			return err
		}
	}
	return nil
}
