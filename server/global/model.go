package global

import (
	"time"

	"gorm.io/gorm"
)

type GVA_MODEL struct {
	ID        uint           `gorm:"primarykey"` // primary key ID
	CreatedAt time.Time      // creation time
	UpdatedAt time.Time      // update time
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"` // delete time
}
