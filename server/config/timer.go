package config

type Timer struct {
	Start       bool     `mapstructure:"start" json:"start" yaml:"start"`                      // when to enable
	Spec        string   `mapstructure:"spec" json:"spec" yaml:"spec"`                         // CRON expression
	WithSeconds bool     `mapstructure:"with_seconds" json:"with_seconds" yaml:"with_seconds"` // Is it accurate to the second
	Detail      []Detail `mapstructure:"detail" json:"detail" yaml:"detail"`
}

type Detail struct {
	TableName    string `mapstructure:"tableName" json:"tableName" yaml:"tableName"`          // The name of the table to clean
	CompareField string `mapstructure:"compareField" json:"compareField" yaml:"compareField"` // Fields that need to compare time
	Interval     string `mapstructure:"interval" json:"interval" yaml:"interval"`             // time interval
}
