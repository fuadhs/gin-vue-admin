package config

type Redis struct {
	DB       int    `mapstructure:"db" json:"db" yaml:"db"`                   // when database of redis
	Addr     string `mapstructure:"addr" json:"addr" yaml:"addr"`             // Server address: port
	Password string `mapstructure:"password" json:"password" yaml:"password"` // password
}
