package config

type Email struct {
	To       string `mapstructure:"to" json:"to" yaml:"to"`                   // Recipients: multiple separated by commas
	Port     int    `mapstructure:"port" json:"port" yaml:"port"`             // port
	From     string `mapstructure:"from" json:"from" yaml:"from"`             // recipient
	Host     string `mapstructure:"host" json:"host" yaml:"host"`             // server address
	IsSSL    bool   `mapstructure:"is-ssl" json:"is-ssl" yaml:"is-ssl"`       // when SSL
	Secret   string `mapstructure:"secret" json:"secret" yaml:"secret"`       // key (password)
	Nickname string `mapstructure:"nickname" json:"nickname" yaml:"nickname"` // Nick name
}
