package config

type DsnProvider interface {
	Dsn() string
}

// The Embeded structure can be flattened to the previous level, thus keeping the structure of the config file as it is
// See playground: https://go.dev/play/p/KIcuhqEoxmY

// GeneralDB is also used as-is by Pgsql and Mysql
type GeneralDB struct {
	Path         string `mapstructure:"path" json:"path" yaml:"path"`                               // Server address: port
	Port         string `mapstructure:"port" json:"port" yaml:"port"`                               // :port
	Config       string `mapstructure:"config" json:"config" yaml:"config"`                         // advanced configuration
	Dbname       string `mapstructure:"db-name" json:"db-name" yaml:"db-name"`                      // Database name
	Username     string `mapstructure:"username" json:"username" yaml:"username"`                   // database username
	Password     string `mapstructure:"password" json:"password" yaml:"password"`                   // database password
	Prefix       string `mapstructure:"prefix" json:"prefix" yaml:"prefix"`                         // Global table prefix, if TableName is defined separately, it will not take effect
	Singular     bool   `mapstructure:"singular" json:"singular" yaml:"singular"`                   // Whether to enable global disable plurals, true means enabled
	Engine       string `mapstructure:"engine" json:"engine" yaml:"engine" default:"InnoDB"`        // Database engine, default InnoDB
	MaxIdleConns int    `mapstructure:"max-idle-conns" json:"max-idle-conns" yaml:"max-idle-conns"` // Maximum number of idle connections
	MaxOpenConns int    `mapstructure:"max-open-conns" json:"max-open-conns" yaml:"max-open-conns"` // Maximum number of connections to open to the database
	LogMode      string `mapstructure:"log-mode" json:"log-mode" yaml:"log-mode"`                   // Whether to enable Gorm global log
	LogZap       bool   `mapstructure:"log-zap" json:"log-zap" yaml:"log-zap"`                      // Whether to write log files through zap
}

type SpecializedDB struct {
	Disable   bool   `mapstructure:"disable" json:"disable" yaml:"disable"`
	Type      string `mapstructure:"type" json:"type" yaml:"type"`
	AliasName string `mapstructure:"alias-name" json:"alias-name" yaml:"alias-name"`
	GeneralDB `yaml:",inline" mapstructure:",squash"`
}
