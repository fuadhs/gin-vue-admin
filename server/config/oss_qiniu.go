package config

type Qiniu struct {
	Zone          string `mapstructure:"zone" json:"zone" yaml:"zone"`                                  // storage area
	Bucket        string `mapstructure:"bucket" json:"bucket" yaml:"bucket"`                            // space name
	ImgPath       string `mapstructure:"img-path" json:"img-path" yaml:"img-path"`                      // CDN accelerated domain name
	UseHTTPS      bool   `mapstructure:"use-https" json:"use-https" yaml:"use-https"`                   // Whether to use https
	AccessKey     string `mapstructure:"access-key" json:"access-key" yaml:"access-key"`                // Key AK
	SecretKey     string `mapstructure:"secret-key" json:"secret-key" yaml:"secret-key"`                // secret key SK
	UseCdnDomains bool   `mapstructure:"use-cdn-domains" json:"use-cdn-domains" yaml:"use-cdn-domains"` // Whether to use CDN upload acceleration for uploading
}
