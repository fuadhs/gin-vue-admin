package middleware

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

// LogLayout 日志layout
type LogLayout struct {
	Time      time.Time
	Metadata  map[string]interface{} // Store custom metadata
	Path      string                 // access path
	Query     string                 // Query
	Body      string                 // Body
	IP        string                 // IP address
	UserAgent string                 // UserAgent
	Error     string                 // Error
	Cost      time.Duration          // Cost
	Source    string                 // Source
}

type Logger struct {
	// Filter user-defined filter
	Filter func(c *gin.Context) bool
	// FilterKeyword Keyword filtering (key)
	FilterKeyword func(layout *LogLayout) bool
	// AuthProcess Authentication processing
	AuthProcess func(c *gin.Context, layout *LogLayout)
	// log processing
	Print func(LogLayout)
	// Source service unique identifier
	Source string
}

func (l Logger) SetLoggerMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		path := c.Request.URL.Path
		query := c.Request.URL.RawQuery
		var body []byte
		if l.Filter != nil && !l.Filter(c) {
			body, _ = c.GetRawData()
			// Put the original body back
			c.Request.Body = io.NopCloser(bytes.NewBuffer(body))
		}
		c.Next()
		cost := time.Since(start)
		layout := LogLayout{
			Time:      time.Now(),
			Path:      path,
			Query:     query,
			IP:        c.ClientIP(),
			UserAgent: c.Request.UserAgent(),
			Error:     strings.TrimRight(c.Errors.ByType(gin.ErrorTypePrivate).String(), "\n"),
			Cost:      cost,
			Source:    l.Source,
		}
		if l.Filter != nil && !l.Filter(c) {
			layout.Body = string(body)
		}
		if l.AuthProcess != nil {
			// Information required for processing authentication
			l.AuthProcess(c, &layout)
		}
		if l.FilterKeyword != nil {
			// Self-judgment key/value desensitization, etc.
			l.FilterKeyword(&layout)
		}
		// Handle logs yourself
		l.Print(layout)
	}
}

func DefaultLogger() gin.HandlerFunc {
	return Logger{
		Print: func(layout LogLayout) {
			// 标准output,k8s做收集
			v, _ := json.Marshal(layout)
			fmt.Println(string(v))
		},
		Source: "GVA",
	}.SetLoggerMiddleware()
}
