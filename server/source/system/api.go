package system

import (
	"context"

	sysModel "github.com/flipped-aurora/gin-vue-admin/server/model/system"
	"github.com/flipped-aurora/gin-vue-admin/server/service/system"
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

type initApi struct{}

const initOrderApi = system.InitOrderSystem + 1

// auto run
func init() {
	system.RegisterInit(initOrderApi, &initApi{})
}

func (i initApi) InitializerName() string {
	return sysModel.SysApi{}.TableName()
}

func (i *initApi) MigrateTable(ctx context.Context) (context.Context, error) {
	db, ok := ctx.Value("db").(*gorm.DB)
	if !ok {
		return ctx, system.ErrMissingDBContext
	}
	return ctx, db.AutoMigrate(&sysModel.SysApi{})
}

func (i *initApi) TableCreated(ctx context.Context) bool {
	db, ok := ctx.Value("db").(*gorm.DB)
	if !ok {
		return false
	}
	return db.Migrator().HasTable(&sysModel.SysApi{})
}

func (i *initApi) InitializeData(ctx context.Context) (context.Context, error) {
	db, ok := ctx.Value("db").(*gorm.DB)
	if !ok {
		return ctx, system.ErrMissingDBContext
	}
	entities := []sysModel.SysApi{
		{ApiGroup: "base", Method: "POST", Path: "/base/login", Description: "User login (required)"},

		{ApiGroup: "jwt", Method: "POST", Path: "/jwt/jsonInBlacklist", Description: "Add jwt to the blacklist (exit, required)"},

		{ApiGroup: "System User", Method: "DELETE", Path: "/user/deleteUser", Description: "Delete Users"},
		{ApiGroup: "System User", Method: "POST", Path: "/user/admin_register", Description: "User Registration"},
		{ApiGroup: "System User", Method: "POST", Path: "/user/getUserList", Description: "get user list"},
		{ApiGroup: "System User", Method: "PUT", Path: "/user/setUserInfo", Description: "Set user information"},
		{ApiGroup: "System User", Method: "PUT", Path: "/user/setSelfInfo", Description: "Set your own information (required)"},
		{ApiGroup: "System User", Method: "GET", Path: "/user/getUserInfo", Description: "Obtain your own information (required)"},
		{ApiGroup: "System User", Method: "POST", Path: "/user/setUserAuthorities", Description: "Set permission group"},
		{ApiGroup: "System User", Method: "POST", Path: "/user/changePassword", Description: "Set permission group"},
		{ApiGroup: "System User", Method: "POST", Path: "/user/setUserAuthority", Description: "Modify user Role (required)"},
		{ApiGroup: "System User", Method: "POST", Path: "/user/resetPassword", Description: "reset user password"},

		{ApiGroup: "api", Method: "POST", Path: "/api/createApi", Description: "Create API"},
		{ApiGroup: "api", Method: "POST", Path: "/api/deleteApi", Description: "Delete API"},
		{ApiGroup: "api", Method: "POST", Path: "/api/updateApi", Description: "Update APIs"},
		{ApiGroup: "api", Method: "POST", Path: "/api/getApiList", Description: "Get API list"},
		{ApiGroup: "api", Method: "POST", Path: "/api/getAllApis", Description: "Get All APIs"},
		{ApiGroup: "api", Method: "POST", Path: "/api/getApiById", Description: "Get API Details"},
		{ApiGroup: "api", Method: "DELETE", Path: "/api/deleteApisByIds", Description: "Batch Delete API"},

		{ApiGroup: "Role", Method: "POST", Path: "/authority/copyAuthority", Description: "Copy Role"},
		{ApiGroup: "Role", Method: "POST", Path: "/authority/createAuthority", Description: "Create a role"},
		{ApiGroup: "Role", Method: "POST", Path: "/authority/deleteAuthority", Description: "Delete Role"},
		{ApiGroup: "Role", Method: "PUT", Path: "/authority/updateAuthority", Description: "Update Role information"},
		{ApiGroup: "Role", Method: "POST", Path: "/authority/getAuthorityList", Description: "Get the list of Roles"},
		{ApiGroup: "Role", Method: "POST", Path: "/authority/setDataAuthority", Description: "Set Role resource permissions"},

		{ApiGroup: "casbin", Method: "POST", Path: "/casbin/updateCasbin", Description: "Change Role API permissions"},
		{ApiGroup: "casbin", Method: "POST", Path: "/casbin/getPolicyPathByAuthorityId", Description: "Get permission list"},

		{ApiGroup: "Menu", Method: "POST", Path: "/menu/addBaseMenu", Description: "New Menu"},
		{ApiGroup: "Menu", Method: "POST", Path: "/menu/getMenu", Description: "Get the Menu tree (required)"},
		{ApiGroup: "Menu", Method: "POST", Path: "/menu/deleteBaseMenu", Description: "Delete Menu"},
		{ApiGroup: "Menu", Method: "POST", Path: "/menu/updateBaseMenu", Description: "Update Menu"},
		{ApiGroup: "Menu", Method: "POST", Path: "/menu/getBaseMenuById", Description: "Get Menu by id"},
		{ApiGroup: "Menu", Method: "POST", Path: "/menu/getMenuList", Description: "Paging to get the basic menu list"},
		{ApiGroup: "Menu", Method: "POST", Path: "/menu/getBaseMenuTree", Description: "Get user dynamic routing"},
		{ApiGroup: "Menu", Method: "POST", Path: "/menu/getMenuAuthority", Description: "Get the specified Role Menu"},
		{ApiGroup: "Menu", Method: "POST", Path: "/menu/addMenuAuthority", Description: "Increase the relationship between menu and Role"},

		{ApiGroup: "Upload", Method: "GET", Path: "/fileUploadAndDownload/findFile", Description: "Find the target file (second transmission)"},
		{ApiGroup: "Upload", Method: "POST", Path: "/fileUploadAndDownload/breakpointContinue", Description: "http"},
		{ApiGroup: "Upload", Method: "POST", Path: "/fileUploadAndDownload/breakpointContinueFinish", Description: "Resume upload complete"},
		{ApiGroup: "Upload", Method: "POST", Path: "/fileUploadAndDownload/removeChunk", Description: "Upload complete remove file"},

		{ApiGroup: "File upload and download", Method: "POST", Path: "/fileUploadAndDownload/upload", Description: "File upload example"},
		{ApiGroup: "File upload and download", Method: "POST", Path: "/fileUploadAndDownload/deleteFile", Description: "Delete Files"},
		{ApiGroup: "File upload and download", Method: "POST", Path: "/fileUploadAndDownload/editFileName", Description: "File name or comment editing"},
		{ApiGroup: "File upload and download", Method: "POST", Path: "/fileUploadAndDownload/getFileList", Description: "Get the list of uploaded files"},

		{ApiGroup: "System Service", Method: "POST", Path: "/system/getServerInfo", Description: "Get server information"},
		{ApiGroup: "System Service", Method: "POST", Path: "/system/getSystemConfig", Description: "Get configuration file content"},
		{ApiGroup: "System Service", Method: "POST", Path: "/system/setSystemConfig", Description: "Set configuration file content"},

		{ApiGroup: "Client", Method: "PUT", Path: "/customer/customer", Description: "update customer"},
		{ApiGroup: "Client", Method: "POST", Path: "/customer/customer", Description: "create customer"},
		{ApiGroup: "Client", Method: "DELETE", Path: "/customer/customer", Description: "delete customer"},
		{ApiGroup: "Client", Method: "GET", Path: "/customer/customer", Description: "Get a single customer"},
		{ApiGroup: "Client", Method: "GET", Path: "/customer/customerList", Description: "Get a list of customers"},

		{ApiGroup: "Code generator", Method: "GET", Path: "/autoCode/getDB", Description: "get all databases"},
		{ApiGroup: "Code generator", Method: "GET", Path: "/autoCode/getTables", Description: "Get database table"},
		{ApiGroup: "Code generator", Method: "POST", Path: "/autoCode/createTemp", Description: "automation code"},
		{ApiGroup: "Code generator", Method: "POST", Path: "/autoCode/preview", Description: "Preview automation code"},
		{ApiGroup: "Code generator", Method: "GET", Path: "/autoCode/getColumn", Description: "Get all fields of the selected table"},
		{ApiGroup: "Code generator", Method: "POST", Path: "/autoCode/createPlug", Description: "Automatically create plugin packages"},
		{ApiGroup: "Code generator", Method: "POST", Path: "/autoCode/installPlugin", Description: "install plugin"},

		{ApiGroup: "Package (pkg) Generator", Method: "POST", Path: "/autoCode/createPackage", Description: "Generate package (package)"},
		{ApiGroup: "Package (pkg) Generator", Method: "POST", Path: "/autoCode/getPackage", Description: "Get all packages"},
		{ApiGroup: "Package (pkg) Generator", Method: "POST", Path: "/autoCode/delPackage", Description: "delete package(package)"},

		{ApiGroup: "Code generator history", Method: "POST", Path: "/autoCode/getMeta", Description: "Get meta information"},
		{ApiGroup: "Code generator history", Method: "POST", Path: "/autoCode/rollback", Description: "Rollback auto-generated code"},
		{ApiGroup: "Code generator history", Method: "POST", Path: "/autoCode/getSysHistory", Description: "Query rollback records"},
		{ApiGroup: "Code generator history", Method: "POST", Path: "/autoCode/delSysHistory", Description: "delete rollback record"},

		{ApiGroup: "System Dictionary Details", Method: "PUT", Path: "/sysDictionaryDetail/updateSysDictionaryDetail", Description: "update dictionary content"},
		{ApiGroup: "System Dictionary Details", Method: "POST", Path: "/sysDictionaryDetail/createSysDictionaryDetail", Description: "Add dictionary content"},
		{ApiGroup: "System Dictionary Details", Method: "DELETE", Path: "/sysDictionaryDetail/deleteSysDictionaryDetail", Description: "Delete dictionary content"},
		{ApiGroup: "System Dictionary Details", Method: "GET", Path: "/sysDictionaryDetail/findSysDictionaryDetail", Description: "Get dictionary content by ID"},
		{ApiGroup: "System Dictionary Details", Method: "GET", Path: "/sysDictionaryDetail/getSysDictionaryDetailList", Description: "Get list of dictionary contents"},

		{ApiGroup: "System Dictionary", Method: "POST", Path: "/sysDictionary/createSysDictionary", Description: "add dictionary"},
		{ApiGroup: "System Dictionary", Method: "DELETE", Path: "/sysDictionary/deleteSysDictionary", Description: "delete dictionary"},
		{ApiGroup: "System Dictionary", Method: "PUT", Path: "/sysDictionary/updateSysDictionary", Description: "update dictionary"},
		{ApiGroup: "System Dictionary", Method: "GET", Path: "/sysDictionary/findSysDictionary", Description: "Get dictionary by ID"},
		{ApiGroup: "System Dictionary", Method: "GET", Path: "/sysDictionary/getSysDictionaryList", Description: "Get list of dictionaries"},

		{ApiGroup: "Operation Record", Method: "POST", Path: "/sysOperationRecord/createSysOperationRecord", Description: "Add operation record"},
		{ApiGroup: "Operation Record", Method: "GET", Path: "/sysOperationRecord/findSysOperationRecord", Description: "Get operation records by ID"},
		{ApiGroup: "Operation Record", Method: "GET", Path: "/sysOperationRecord/getSysOperationRecordList", Description: "Get a list of operation records"},
		{ApiGroup: "Operation Record", Method: "DELETE", Path: "/sysOperationRecord/deleteSysOperationRecord", Description: "Delete operation record"},
		{ApiGroup: "Operation Record", Method: "DELETE", Path: "/sysOperationRecord/deleteSysOperationRecordByIds", Description: "Batch delete operation history"},

		{ApiGroup: "Breakpoint resume (plug-in version)", Method: "POST", Path: "/simpleUploader/upload", Description: "Plug-in version multipart upload"},
		{ApiGroup: "Breakpoint resume (plug-in version)", Method: "GET", Path: "/simpleUploader/checkFileMd5", Description: "File Integrity Verification"},
		{ApiGroup: "Breakpoint resume (plug-in version)", Method: "GET", Path: "/simpleUploader/mergeFileMd5", Description: "Upload the merged file"},

		{ApiGroup: "email", Method: "POST", Path: "/email/emailTest", Description: "Send test email"},
		{ApiGroup: "email", Method: "POST", Path: "/email/emailSend", Description: "Example of sending mail"},

		{ApiGroup: "Button Permissions", Method: "POST", Path: "/authorityBtn/setAuthorityBtn", Description: "Set button permissions"},
		{ApiGroup: "Button Permissions", Method: "POST", Path: "/authorityBtn/getAuthorityBtn", Description: "Get existing button permissions"},
		{ApiGroup: "Button Permissions", Method: "POST", Path: "/authorityBtn/canRemoveAuthorityBtn", Description: "delete button"},
	}
	if err := db.Create(&entities).Error; err != nil {
		return ctx, errors.Wrap(err, sysModel.SysApi{}.TableName()+"Table data initialization failed!")
	}
	next := context.WithValue(ctx, i.InitializerName(), entities)
	return next, nil
}

func (i *initApi) DataInserted(ctx context.Context) bool {
	db, ok := ctx.Value("db").(*gorm.DB)
	if !ok {
		return false
	}
	if errors.Is(db.Where("path = ? AND method = ?", "/authorityBtn/canRemoveAuthorityBtn", "POST").
		First(&sysModel.SysApi{}).Error, gorm.ErrRecordNotFound) {
		return false
	}
	return true
}
