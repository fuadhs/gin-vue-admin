package example

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	"github.com/flipped-aurora/gin-vue-admin/server/model/system"
)

type ExaCustomer struct {
	global.GVA_MODEL
	CustomerName       string         `json:"customerName" form:"customerName" gorm:"comment:customerName"`                   // customerName
	CustomerPhoneData  string         `json:"customerPhoneData" form:"customerPhoneData" gorm:"comment:customerPhoneData"`    // customerPhoneData
	SysUserID          uint           `json:"sysUserId" form:"sysUserId" gorm:"comment:sysUserId"`                            // sysUserId
	SysUserAuthorityID uint           `json:"sysUserAuthorityID" form:"sysUserAuthorityID" gorm:"comment:sysUserAuthorityID"` // sysUserAuthorityID
	SysUser            system.SysUser `json:"sysUser" form:"sysUser" gorm:"comment:sysUser"`                                  // sysUser
}
