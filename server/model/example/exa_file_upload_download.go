package example

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

type ExaFileUploadAndDownload struct {
	global.GVA_MODEL
	Name string `json:"name" gorm:"comment:name"` // name
	Url  string `json:"url" gorm:"comment:url"`   // url
	Tag  string `json:"tag" gorm:"comment:tag"`   // tag
	Key  string `json:"key" gorm:"comment:key"`   // key
}

func (ExaFileUploadAndDownload) TableName() string {
	return "exa_file_upload_and_downloads"
}
