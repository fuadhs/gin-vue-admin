package system

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

type SysApi struct {
	global.GVA_MODEL
	Path        string `json:"path" gorm:"comment:apipath"`                 // api path
	Description string `json:"description" gorm:"comment:api  description"` // api  description
	ApiGroup    string `json:"apiGroup" gorm:"comment:api group"`           // api group
	Method      string `json:"method" gorm:"default:POST;comment:method"`   // method: create POST (default) | view GET | update PUT | delete DELETE
}

func (SysApi) TableName() string {
	return "sys_apis"
}
