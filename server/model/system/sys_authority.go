package system

import (
	"time"
)

type SysAuthority struct {
	CreatedAt       time.Time       // creation time
	UpdatedAt       time.Time       // update time
	DeletedAt       *time.Time      `sql:"index"`
	AuthorityId     uint            `json:"authorityId" gorm:"not null;unique;primary_key;comment:character ID;size:90"` // character ID
	AuthorityName   string          `json:"authorityName" gorm:"comment:AuthorityName"`                                  // AuthorityName
	ParentId        *uint           `json:"parentId" gorm:"comment:parent character ID"`                                 // parent character ID
	DataAuthorityId []*SysAuthority `json:"dataAuthorityId" gorm:"many2many:sys_data_authority_id;"`
	Children        []SysAuthority  `json:"children" gorm:"-"`
	SysBaseMenus    []SysBaseMenu   `json:"menus" gorm:"many2many:sys_authority_menus;"`
	Users           []SysUser       `json:"-" gorm:"many2many:sys_user_authority;"`
	DefaultRouter   string          `json:"defaultRouter" gorm:"comment:default menu;default:dashboard"` // default menu (default dashboard)
}

func (SysAuthority) TableName() string {
	return "sys_authorities"
}
