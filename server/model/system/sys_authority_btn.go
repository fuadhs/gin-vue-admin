package system

type SysAuthorityBtn struct {
	AuthorityId      uint           `gorm:"comment:character ID"`
	SysMenuID        uint           `gorm:"comment:menu id"`
	SysBaseMenuBtnID uint           `gorm:"comment:menu button id"`
	SysBaseMenuBtn   SysBaseMenuBtn ` gorm:"comment:button details"`
}
