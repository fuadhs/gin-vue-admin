// Automatically generate template SysDictionaryDetail
package system

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// If it contains time.Time, please import the time package yourself
type SysDictionaryDetail struct {
	global.GVA_MODEL
	Label           string `json:"label" form:"label" gorm:"column:label;comment:label"`                                            // label
	Value           int    `json:"value" form:"value" gorm:"column:value;comment:value"`                                            // value
	Status          *bool  `json:"status" form:"status" gorm:"column:status;comment:enabled state"`                                 // enabled state
	Sort            int    `json:"sort" form:"sort" gorm:"column:sort;comment:sort mark"`                                           // sort mark
	SysDictionaryID int    `json:"sysDictionaryID" form:"sysDictionaryID" gorm:"column:sys_dictionary_id;comment:association mark"` // association mark
}

func (SysDictionaryDetail) TableName() string {
	return "sys_dictionary_details"
}
