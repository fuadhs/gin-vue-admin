package system

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
	uuid "github.com/satori/go.uuid"
)

type SysUser struct {
	global.GVA_MODEL
	UUID        uuid.UUID      `json:"uuid" gorm:"index;comment:User UUID"`                                                             // User UUID
	Username    string         `json:"userName" gorm:"index;comment:User login名"`                                                       // User login名
	Password    string         `json:"-"  gorm:"comment:User loginpassword"`                                                            // User loginpassword
	NickName    string         `json:"nickName" gorm:"default:system user;comment:user Nick name"`                                      // user Nick name
	SideMode    string         `json:"sideMode" gorm:"default:dark;comment:User Side Theme"`                                            // User Side Theme
	HeaderImg   string         `json:"headerImg" gorm:"default:https://qmplusimg.henrongyi.top/gva_header.jpg;comment:profile picture"` // profile picture
	BaseColor   string         `json:"baseColor" gorm:"default:#fff;comment:base color"`                                                // base color
	ActiveColor string         `json:"activeColor" gorm:"default:#1890ff;comment:active color"`                                         // active color
	AuthorityId uint           `json:"authorityId" gorm:"default:888;comment:usercharacter ID"`                                         // usercharacter ID
	Authority   SysAuthority   `json:"authority" gorm:"foreignKey:AuthorityId;references:AuthorityId;comment:user role"`
	Authorities []SysAuthority `json:"authorities" gorm:"many2many:sys_user_authority;"`
	Phone       string         `json:"phone"  gorm:"comment:User phone number"`                                      // User phone number
	Email       string         `json:"email"  gorm:"comment:user email"`                                             // user email
	Enable      int            `json:"enable" gorm:"default:1;comment:Whether the user is frozen 1 normal 2 frozen"` //Whether the user is frozen 1 normal 2 frozen
}

func (SysUser) TableName() string {
	return "sys_users"
}
