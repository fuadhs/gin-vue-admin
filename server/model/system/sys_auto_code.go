package system

import (
	"errors"
	"go/token"
	"strings"

	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// AutoCodeStruct Initial release of Automated Code Tool
type AutoCodeStruct struct {
	StructName         string   `json:"structName"`         // Struct name
	TableName          string   `json:"tableName"`          // tableName
	PackageName        string   `json:"packageName"`        // packageName
	HumpPackageName    string   `json:"humpPackageName"`    // gopackageName
	Abbreviation       string   `json:"abbreviation"`       // Struct for short
	Description        string   `json:"description"`        // description
	AutoCreateApiToSql bool     `json:"autoCreateApiToSql"` // automatically create api
	AutoCreateResource bool     `json:"autoCreateResource"` // autoCreateResource
	AutoMoveFile       bool     `json:"autoMoveFile"`       // autoMoveFile
	BusinessDB         string   `json:"businessDB"`         // businessDB
	Fields             []*Field `json:"fields,omitempty"`
	HasTimer           bool
	DictTypes          []string `json:"-"`
	Package            string   `json:"package"`
	PackageT           string   `json:"-"`
	NeedValid          bool     `json:"-"`
	NeedSort           bool     `json:"-"`
}

func (a *AutoCodeStruct) Pretreatment() {
	a.KeyWord()
	a.SuffixTest()
}

// KeyWord It is the processing of gokeywords plus _ to prevent compilation errors
// Author [SliverHorn](https://github.com/SliverHorn)
func (a *AutoCodeStruct) KeyWord() {
	if token.IsKeyword(a.Abbreviation) {
		a.Abbreviation = a.Abbreviation + "_"
	}
}

// SuffixTest Handle _test suffix
// Author [SliverHorn](https://github.com/SliverHorn)
func (a *AutoCodeStruct) SuffixTest() {
	if strings.HasSuffix(a.HumpPackageName, "test") {
		a.HumpPackageName = a.HumpPackageName + "_"
	}
}

type Field struct {
	FieldName       string `json:"fieldName"`       // fieldName
	FieldDesc       string `json:"fieldDesc"`       // fieldDesc
	FieldType       string `json:"fieldType"`       // fieldType
	FieldJson       string `json:"fieldJson"`       // FieldJson
	DataTypeLong    string `json:"dataTypeLong"`    // dataTypeLong
	Comment         string `json:"comment"`         // comment
	ColumnName      string `json:"columnName"`      // columnName
	FieldSearchType string `json:"fieldSearchType"` // fieldSearchType
	DictType        string `json:"dictType"`        // dictType
	Require         bool   `json:"require"`         // require
	ErrorText       string `json:"errorText"`       // errorText
	Clearable       bool   `json:"clearable"`       // clearable
	Sort            bool   `json:"sorrt"`           // sorrt
}

var ErrAutoMove error = errors.New("create code successfully and move file successfully")

type SysAutoCode struct {
	global.GVA_MODEL
	PackageName string `json:"packageName" gorm:"comment:Package names"`
	Label       string `json:"label" gorm:"comment:label"`
	Desc        string `json:"desc" gorm:"comment:desc"`
}

type AutoPlugReq struct {
	PlugName    string         `json:"plugName"` // must begin with capital
	Snake       string         `json:"snake"`    // The backend is automatically converted to snake
	RouterGroup string         `json:"routerGroup"`
	HasGlobal   bool           `json:"hasGlobal"`
	HasRequest  bool           `json:"hasRequest"`
	HasResponse bool           `json:"hasResponse"`
	NeedModel   bool           `json:"needModel"`
	Global      []AutoPlugInfo `json:"global,omitempty"`
	Request     []AutoPlugInfo `json:"request,omitempty"`
	Response    []AutoPlugInfo `json:"response,omitempty"`
}

func (a *AutoPlugReq) CheckList() {
	a.Global = bind(a.Global)
	a.Request = bind(a.Request)
	a.Response = bind(a.Response)

}
func bind(req []AutoPlugInfo) []AutoPlugInfo {
	var r []AutoPlugInfo
	for _, info := range req {
		if info.Effective() {
			r = append(r, info)
		}
	}
	return r
}

type AutoPlugInfo struct {
	Key  string `json:"key"`
	Type string `json:"type"`
	Desc string `json:"desc"`
}

func (a AutoPlugInfo) Effective() bool {
	return a.Key != "" && a.Type != "" && a.Desc != ""
}
