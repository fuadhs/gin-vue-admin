package system

import (
	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

type SysBaseMenu struct {
	global.GVA_MODEL
	MenuLevel     uint                                                        `json:"-"`
	ParentId      string                                                      `json:"parentId" gorm:"comment:parent menu id"`                     // parent menu id
	Path          string                                                      `json:"path" gorm:"comment:routing path"`                           // routing path
	Name          string                                                      `json:"name" gorm:"comment:route name"`                             // route name
	Hidden        bool                                                        `json:"hidden" gorm:"comment:when hide in the list"`                // when hide in the list
	Component     string                                                      `json:"component" gorm:"comment:Corresponding front-end file path"` // Corresponding front-end file path
	Sort          int                                                         `json:"sort" gorm:"comment:sort mark"`                              // sort mark
	Meta          `json:"meta" gorm:"embedded;comment:Additional attributes"` // Additional attributes
	SysAuthoritys []SysAuthority                                              `json:"authoritys" gorm:"many2many:sys_authority_menus;"`
	Children      []SysBaseMenu                                               `json:"children" gorm:"-"`
	Parameters    []SysBaseMenuParameter                                      `json:"parameters"`
	MenuBtn       []SysBaseMenuBtn                                            `json:"menuBtn"`
}

type Meta struct {
	ActiveName  string `json:"activeName" gorm:"comment:highlight menu"`
	KeepAlive   bool   `json:"keepAlive" gorm:"comment:enable to cache"`                           // enable to cache
	DefaultMenu bool   `json:"defaultMenu" gorm:"comment:Is it a basic route (under development)"` // Is it a basic route (under development)
	Title       string `json:"title" gorm:"comment:title"`                                         // title
	Icon        string `json:"icon" gorm:"comment:icon"`                                           // icon
	CloseTab    bool   `json:"closeTab" gorm:"comment:close tab automatically"`                    // close tab automatically
}

type SysBaseMenuParameter struct {
	global.GVA_MODEL
	SysBaseMenuID uint
	Type          string `json:"type" gorm:"comment:when the parameter carried in the address bar is params or query"` // when the parameter carried in the address bar is params or query
	Key           string `json:"key" gorm:"comment:The address bar carries the key of the parameter"`                  // The address bar carries the key of the parameter
	Value         string `json:"value" gorm:"comment:The address bar carries the value of the parameter"`              // The address bar carries the value of the parameter
}

func (SysBaseMenu) TableName() string {
	return "sys_base_menus"
}
