package request

import (
	"github.com/flipped-aurora/gin-vue-admin/server/model/system"
)

// Register User register structure
type Register struct {
	Username     string `json:"userName" example:"username"`
	Password     string `json:"passWord" example:"password"`
	NickName     string `json:"nickName" example:"Nick name"`
	HeaderImg    string `json:"headerImg" example:"Avatar link"`
	AuthorityId  uint   `json:"authorityId" swaggertype:"string" example:"int character ID"`
	Enable       int    `json:"enable" swaggertype:"string" example:"int when to enable"`
	AuthorityIds []uint `json:"authorityIds" swaggertype:"string" example:"[]uint character ID"`
	Phone        string `json:"phone" example:"telephone number"`
	Email        string `json:"email" example:"E-mail"`
}

// User login structure
type Login struct {
	Username  string `json:"username"`  // username
	Password  string `json:"password"`  // password
	Captcha   string `json:"captcha"`   // verification code
	CaptchaId string `json:"captchaId"` // verification codeID
}

// Modify password structure
type ChangePasswordReq struct {
	ID          uint   `json:"-"`           // Extract user id from JWT to avoid unauthorized access
	Password    string `json:"password"`    // password
	NewPassword string `json:"newPassword"` // 新password
}

// Modify  user's auth structure
type SetUserAuth struct {
	AuthorityId uint `json:"authorityId"` // character ID
}

// Modify  user's auth structure
type SetUserAuthorities struct {
	ID           uint
	AuthorityIds []uint `json:"authorityIds"` // character ID
}

type ChangeUserInfo struct {
	ID           uint                  `gorm:"primarykey"`                                                                                      // primary key ID
	NickName     string                `json:"nickName" gorm:"default:system user;comment:user Nickname"`                                       // user Nick name
	Phone        string                `json:"phone"  gorm:"comment:User phone number"`                                                         // User phone number
	AuthorityIds []uint                `json:"authorityIds" gorm:"-"`                                                                           // character ID
	Email        string                `json:"email"  gorm:"comment:user email"`                                                                // user email
	HeaderImg    string                `json:"headerImg" gorm:"default:https://qmplusimg.henrongyi.top/gva_header.jpg;comment:profile picture"` // profile picture
	SideMode     string                `json:"sideMode"  gorm:"comment:User Side Theme"`                                                        // User Side Theme
	Enable       int                   `json:"enable" gorm:"comment:Status Enable-disable"`                                                     //Status Enable-disable
	Authorities  []system.SysAuthority `json:"-" gorm:"many2many:sys_user_authority;"`
}
