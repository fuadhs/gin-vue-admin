// 自动生成模板SysOperationRecord
package system

import (
	"time"

	"github.com/flipped-aurora/gin-vue-admin/server/global"
)

// If it contains time.Time, please import the time package yourself
type SysOperationRecord struct {
	global.GVA_MODEL
	Ip           string        `json:"ip" form:"ip" gorm:"column:ip;comment:ip address"`                                     // ip address
	Method       string        `json:"method" form:"method" gorm:"column:method;comment:request method"`                     // request method
	Path         string        `json:"path" form:"path" gorm:"column:path;comment:request path"`                             // request path
	Status       int           `json:"status" form:"status" gorm:"column:status;comment:request status"`                     // request status
	Latency      time.Duration `json:"latency" form:"latency" gorm:"column:latency;comment:latency" swaggertype:"string"`    // latency
	Agent        string        `json:"agent" form:"agent" gorm:"column:agent;comment:user-agent"`                            // user-agent
	ErrorMessage string        `json:"error_message" form:"error_message" gorm:"column:error_message;comment:error message"` // error message
	Body         string        `json:"body" form:"body" gorm:"type:text;column:body;comment:request Body"`                   // request Body
	Resp         string        `json:"resp" form:"resp" gorm:"type:text;column:resp;comment:response Body"`                  // response Body
	UserID       int           `json:"user_id" form:"user_id" gorm:"column:user_id;comment:User ID"`                         // User ID
	User         SysUser       `json:"user"`
}
