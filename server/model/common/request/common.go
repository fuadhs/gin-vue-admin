package request

// PageInfo Paging common input parameter structure
type PageInfo struct {
	Page     int    `json:"page" form:"page"`         // page number
	PageSize int    `json:"pageSize" form:"pageSize"` // page size
	Keyword  string `json:"keyword" form:"keyword"`   //keywords
}

// GetById Find by id structure
type GetById struct {
	ID int `json:"id" form:"id"` // primary key ID
}

func (r *GetById) Uint() uint {
	return uint(r.ID)
}

type IdsReq struct {
	Ids []int `json:"ids" form:"ids"`
}

// GetAuthorityId Get role by id structure
type GetAuthorityId struct {
	AuthorityId uint `json:"authorityId" form:"authorityId"` // character ID
}

type Empty struct{}
