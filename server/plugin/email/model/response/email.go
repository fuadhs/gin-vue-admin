package response

type Email struct {
	To      string `json:"to"`      // to
	Subject string `json:"subject"` // subject
	Body    string `json:"body"`    // body
}
