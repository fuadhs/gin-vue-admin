package config

type Email struct {
	To       string `mapstructure:"to" json:"to" yaml:"to"`                   // Recipients: multiple separated by commas Example: a@qq.com b@qq.com Please use this item as a parameter during official development
	From     string `mapstructure:"from" json:"from" yaml:"from"`             // Sender Your own email address
	Host     string `mapstructure:"host" json:"host" yaml:"host"`             // server address For example, smtp.qq.com Please go to QQ or the mailbox you want to send email to check its smtp protocol
	Secret   string `mapstructure:"secret" json:"secret" yaml:"secret"`       // key (password) The key (password) used for login. It is best not to use the email password. Go to the email smtp to apply for a key (password) for login
	Nickname string `mapstructure:"nickname" json:"nickname" yaml:"nickname"` // Nick name The sender Nick name is usually your own mailbox
	Port     int    `mapstructure:"port" json:"port" yaml:"port"`             // port Please go to QQ or the mailbox you want to send email to check its smtp protocol, most of them are 465
	IsSSL    bool   `mapstructure:"is-ssl" json:"isSSL" yaml:"is-ssl"`        // when SSL Whether to enable SSL
}
