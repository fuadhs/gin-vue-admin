import service from '@/utils/request'

// @Tags api
// @Summary Paging to get the list of roles
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "Paging to get user list"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"get success"}"
// @Router /api/getApiList [post]
// {
//  page     int
//	pageSize int
// }
export const getApiList = (data) => {
  return service({
    url: '/api/getApiList',
    method: 'post',
    data
  })
}

// @Tags Api
// @Summary Create basic APIs
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateApiParams true "createapi"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"get success"}"
// @Router /api/createApi [post]
export const createApi = (data) => {
  return service({
    url: '/api/createApi',
    method: 'post',
    data
  })
}

// @Tags menu
// @Summary get menu by id
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.GetById true "get menu by id"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"get success"}"
// @Router /menu/getApiById [post]
export const getApiById = (data) => {
  return service({
    url: '/api/getApiById',
    method: 'post',
    data
  })
}

// @Tags Api
// @Summary updateapi
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateApiParams true "updateapi"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"update completed"}"
// @Router /api/updateApi [post]
export const updateApi = (data) => {
  return service({
    url: '/api/updateApi',
    method: 'post',
    data
  })
}

// @Tags Api
// @Summary updateapi
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.CreateApiParams true "updateapi"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"update completed"}"
// @Router /api/setAuthApi [post]
export const setAuthApi = (data) => {
  return service({
    url: '/api/setAuthApi',
    method: 'post',
    data
  })
}

// @Tags Api
// @Summary Get all APIs without pagination
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Success 200 {string} json "{"success":true,"data":{},"msg":"get success"}"
// @Router /api/getAllApis [post]
export const getAllApis = (data) => {
  return service({
    url: '/api/getAllApis',
    method: 'post',
    data
  })
}

// @Tags Api
// @Summary delete指定api
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body dbModel.Api true "deleteapi"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"successfully deleted"}"
// @Router /api/deleteApi [post]
export const deleteApi = (data) => {
  return service({
    url: '/api/deleteApi',
    method: 'post',
    data
  })
}

// @Tags SysApi
// @Summary Delete the selected API
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "ID"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"successfully deleted"}"
// @Router /api/deleteApisByIds [delete]
export const deleteApisByIds = (data) => {
  return service({
    url: '/api/deleteApisByIds',
    method: 'delete',
    data
  })
}
