import config from './config'

// Unified import of el-icon icons
import * as ElIconModules from '@element-plus/icons-vue'
//Import the function that converts the icon name

export const register = (app) => {
  // Unified registration el-icon icon
  for (const iconName in ElIconModules) {
    app.component(iconName, ElIconModules[iconName])
  }
  app.config.globalProperties.$GIN_VUE_ADMIN = config
}
