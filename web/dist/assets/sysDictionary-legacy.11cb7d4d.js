/*! 
 Build based on gin-vue-admin 
 Time : 1678334030000 */
System.register(["./index-legacy.4205a2db.js"],(function(t,r){"use strict";var e;return{setters:[function(t){e=t.aJ}],execute:function(){t("c",(function(t){return e({url:"/sysDictionary/createSysDictionary",method:"post",data:t})})),t("d",(function(t){return e({url:"/sysDictionary/deleteSysDictionary",method:"delete",data:t})})),t("u",(function(t){return e({url:"/sysDictionary/updateSysDictionary",method:"put",data:t})})),t("f",(function(t){return e({url:"/sysDictionary/findSysDictionary",method:"get",params:t})})),t("g",(function(t){return e({url:"/sysDictionary/getSysDictionaryList",method:"get",params:t})}))}}}));
